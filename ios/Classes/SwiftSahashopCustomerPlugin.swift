import Flutter
import UIKit
import FBSDKCoreKit
import FBSDKShareKit

public class SwiftSahashopCustomerPlugin: NSObject, FlutterPlugin {
   
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "sahashop_customer", binaryMessenger: registrar.messenger())
    let instance = SwiftSahashopCustomerPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
      switch call.method {
         case "getName":
          if let arguments = call.arguments as? [String: Any],
                    let imageUrls = arguments["image_urls"] as? [String], let url = arguments["url"] as? String {
              result(shareFacebook(listImage:imageUrls, url: url))
                 } else {
                     result(FlutterError(code: "INVALID_ARGUMENT", message: "Invalid arguments", details: nil))
                 }
         default:
             result("iOS " + UIDevice.current.systemVersion)
         }
       }
    


    
func shareFacebook(listImage : [String],url : String)-> String{
        let url = URL(string: url)
        var sharePhotos: [SharePhoto] = []
        let dispatchGroup = DispatchGroup()
        var subImageList = listImage.prefix(6)
        // Tải ảnh từ mỗi URL trong một dispatch group
        for imageUrl in subImageList {
            guard let url = URL(string: imageUrl) else {
                continue
            }
            
            dispatchGroup.enter()
            downloadImage(url: url) { (image) in
                defer {
                    dispatchGroup.leave()
                }
                
                if let image = image {
                    let sharePhoto = SharePhoto(image: image, isUserGenerated: true)
                    
                    sharePhotos.append(sharePhoto)
                }
            }
        }
        
//        func downloadImage(from url: URL, completion: @escaping (UIImage?) -> Void) {
//            URLSession.shared.dataTask(with: url) { (data, response, error) in
//                guard let data = data, let image = UIImage(data: data) else {
//                    completion(nil)
//                    return
//                }
//                completion(image)
//            }.resume()
//        }

        
       
           
         
           
           // Đợi cho tất cả các tác vụ tải ảnh hoàn thành trước khi chia sẻ
           dispatchGroup.notify(queue: .main) {
               // Tạo nội dung chia sẻ
               let content = SharePhotoContent()
               //content.contentURL = url
               content.photos = sharePhotos

               let dialog = ShareDialog(viewController: UIApplication.shared.keyWindow?.rootViewController, content: content, delegate: nil)
               dialog.show()
           }
        return "Nhớ ánh"
        
    }
    func downloadImage(url: URL, completion: @escaping (UIImage?) -> Void) {
           let session = URLSession.shared
           let task = session.dataTask(with: url) { (data, response, error) in
               if let error = error {
                   print("Error loading image: \(error.localizedDescription)")
                   completion(nil)
                   return
               }
               
               guard let data = data else {
                   print("No data received")
                   completion(nil)
                   return
               }
               
               if let image = UIImage(data: data) {
                   completion(image)
               } else {
                   print("Invalid image data")
                   completion(nil)
               }
           }
           task.resume()
       }
   
    
    
    //result("iOS " + UIDevice.current.systemVersion)
  }


