



class ShipmentMethod {
  ShipmentMethod({
    this.partnerId,
    this.fee,
    this.name,
    this.description,
    this.shipType,
  });

  int? partnerId;
  double? fee;
  String? name;
  String? description;
  dynamic shipType;

  factory ShipmentMethod.fromJson(Map<String, dynamic> json) => ShipmentMethod(
        partnerId: json["partner_id"],
        fee: json["fee"] == null ? null : json["fee"].toDouble(),
        name: json["name"],
        description: json["description"],
        shipType: json["ship_type"],
      );

  Map<String, dynamic> toJson() => {
        "partner_id": partnerId,
        "fee": fee,
        "name": name,
        "ship_type": shipType,
      };
}
