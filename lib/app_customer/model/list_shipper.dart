class ListShipper {
    int? partnerId;
    String? name;
    String? logo;

    ListShipper({
        this.partnerId,
        this.name,
        this.logo,
    });

    factory ListShipper.fromJson(Map<String, dynamic> json) => ListShipper(
        partnerId: json["partner_id"],
        name: json["name"],
        logo: json["logo"],
    );

    Map<String, dynamic> toJson() => {
        "partner_id": partnerId,
        "name": name,
        "logo": logo,
    };
}
