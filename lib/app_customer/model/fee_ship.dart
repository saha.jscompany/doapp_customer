class FeeShip {
    int? partnerId;
    String? name;
    List<FeeWithTypeShip>? feeWithTypeShip;

    FeeShip({
        this.partnerId,
        this.name,
        this.feeWithTypeShip,
    });

    factory FeeShip.fromJson(Map<String, dynamic> json) => FeeShip(
        partnerId: json["partner_id"],
        name: json["name"],
        feeWithTypeShip: json["fee_with_type_ship"] == null ? [] : List<FeeWithTypeShip>.from(json["fee_with_type_ship"]!.map((x) => FeeWithTypeShip.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "partner_id": partnerId,
        "name": name,
        "fee_with_type_ship": feeWithTypeShip == null ? [] : List<dynamic>.from(feeWithTypeShip!.map((x) => x.toJson())),
    };
}

class FeeWithTypeShip {
    dynamic shipSpeedCode;
    String? description;
    double? fee;

    FeeWithTypeShip({
        this.shipSpeedCode,
        this.description,
        this.fee,
    });

    factory FeeWithTypeShip.fromJson(Map<String, dynamic> json) => FeeWithTypeShip(
        shipSpeedCode: json["ship_speed_code"],
        description: json["description"],
        fee: json["fee"]?.toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "ship_speed_code": shipSpeedCode,
        "description": description,
        "fee": fee,
    };
}
