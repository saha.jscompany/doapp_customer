import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class GoogleScheetScreen extends StatefulWidget {
  const GoogleScheetScreen({super.key, required this.url});
  final String url;
  @override
  State<GoogleScheetScreen> createState() => _GoogleScheetScreenState();
}

class _GoogleScheetScreenState extends State<GoogleScheetScreen> {
  late final WebViewController _controller;
  @override
  void initState() {
    super.initState();
    final WebViewController controller = WebViewController();
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..loadRequest(Uri.parse(widget.url));
    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Báo giá"),
      ),
      body: WebViewWidget(controller: _controller),
    );
  }
}
