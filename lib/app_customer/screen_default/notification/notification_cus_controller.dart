import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/components/toast/saha_alert.dart';
import 'package:sahashop_customer/app_customer/const/const_type_message.dart';
import 'package:sahashop_customer/app_customer/model/community_post.dart';
import 'package:sahashop_customer/app_customer/model/order.dart';
import 'package:sahashop_customer/app_customer/model/post.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/notification_history/all_notification_response.dart';
import 'package:sahashop_customer/app_customer/repository/repository_customer.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/category_product_screen/category_product_screen.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/product_screen/product_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/category_post_screen/category_post_screen_1.dart';
import 'package:sahashop_customer/app_customer/screen_default/category_post_screen/read_post_screen/input_model_post.dart';
import 'package:sahashop_customer/app_customer/screen_default/category_post_screen/read_post_screen/read_post_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/chat_customer_screen/chat_user_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/ctv_customer/ctv_wallet_screen/ctv_wallet_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/order_history/order_history_detail/order_detail_history_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/order_history/order_history_screen.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../model/product.dart';
import '../community/comment/comment_screen.dart';

class NotificationCusController extends GetxController {
  NotificationCusController() {
    historyNotification();
  }

  var listNotificationCus = RxList<NotificationCus>();
  var isLoadMore = false.obs;
  int currentPage = 1;
  bool isEndOrder = false;
  var isLoadRefresh = true.obs;

  Future<void> readAllNotification({bool? isRefresh}) async {
    try {
      var res = await CustomerRepositoryManager.notificationCusRepository
          .readAllNotification();
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
  }

  Future<void> historyNotification({bool? isRefresh}) async {
    if (isRefresh == true) {
      isLoadRefresh.value = true;
      listNotificationCus([]);
      currentPage = 1;
      isEndOrder = false;
    } else {
      isLoadMore.value = true;
    }

    try {
      if (isEndOrder == false) {
        var res = await CustomerRepositoryManager.notificationCusRepository
            .historyNotification(currentPage);

        res!.data!.listNotification!.data!.forEach((e) {
          listNotificationCus.add(e);
        });

        if (res.data!.listNotification!.nextPageUrl != null) {
          currentPage++;
          isEndOrder = false;
        } else {
          isEndOrder = true;
        }
      } else {
        isLoadMore.value = false;
        return;
      }
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }

    isLoadMore.value = false;
    isLoadRefresh.value = false;
  }

  void navigator(NotificationCus notificationCus) async {
    if (notificationCus.typeAction != null) {
      switch (notificationCus.typeAction) {
        case 'PRODUCT':
          Get.to(() => ProductScreen(
                productId: int.tryParse(notificationCus.valueAction ?? ""),
                product: Product(
                    id: int.tryParse(notificationCus.valueAction ?? "")),
              ));
          break;
        case 'CATEGORY_PRODUCT':
          {
            Get.to(() => CategoryProductScreen(
                  categoryId: int.tryParse(notificationCus.valueAction ?? ""),
                ));
          }
          break;
        case 'POST':
          {
            Get.to(() => ReadPostScreen(
                  inputModelPost: InputModelPost(
                      postId: int.tryParse(notificationCus.valueAction ?? ""),
                      post: Post(
                        id: int.tryParse(notificationCus.valueAction ?? ""),
                      )),
                ));
          }
          break;
        case 'CATEGORY_POST':
          {
            break;
          }
        case 'LINK':
          {
            final url = Uri.parse(notificationCus.valueAction ?? "");
            if (await canLaunchUrl(url)) {
              await launchUrl(url);
            } else {
              SahaAlert.showError(message: "Không thể tải trang này");
            }
            break;
          }

        default:
          {}
      }
    }
    if (notificationCus.type == NEW_COMMENT_POST) {
      Get.to(() => CommentsScreen(
            communityPost: CommunityPost(
                id: int.tryParse(notificationCus.referencesValue ?? "")),
          ));
    }

    if (notificationCus.type == NEW_ORDER) {
      Get.to(() => OrderHistoryScreen())!.then((value) {
        // historyNotification(isRefresh: true);
      });
    }
    if (notificationCus.type == NEW_MESSAGE) {
      Get.to(() => ChatCustomerScreen())!.then((value) {
        //historyNotification(isRefresh: true);
      });
    }
    if (notificationCus.type == NEW_POST) {
      Get.to(() => CategoryPostScreen(
                isAutoBackIcon: true,
              ))!
          .then((value) {
        //historyNotification(isRefresh: true);
      });
    }

    if (notificationCus.type == NEW_PERIODIC_SETTLEMENT) {
      Get.to(() => CtvWalletScreen())!.then((value) {
        //historyNotification(isRefresh: true);
      });
    }

    if ((notificationCus.type ?? "").split("-")[0] == ORDER_STATUS) {
      if ((notificationCus.type ?? "").split("-")[1] ==
          WAITING_FOR_PROGRESSING) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == PACKING) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == SHIPPING) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }
      if ((notificationCus.type ?? "").split("-")[1] == COMPLETED) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == OUT_OF_STOCK) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == USER_CANCELLED) {
        Get.to(() => OrderHistoryScreen(
              initPage: 5,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == CUSTOMER_CANCELLED) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == DELIVERY_ERROR) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == CUSTOMER_RETURNING) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }

      if ((notificationCus.type ?? "").split("-")[1] == CUSTOMER_HAS_RETURNS) {
        Get.to(() => OrderHistoryDetailScreen(
              orderCode: notificationCus.referencesValue,
            ));
      }
    }

    if (notificationCus.type == CUSTOMER_PAID) {
      Get.to(() => OrderHistoryScreen())!.then((value) {
        // historyNotification(isRefresh: true);
      });
    }
  }
}
