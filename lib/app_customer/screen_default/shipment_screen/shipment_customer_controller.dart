import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/model/fee_ship.dart';
import 'package:sahashop_customer/app_customer/model/list_shipper.dart';
import '../../repository/repository_customer.dart';
import '../../components//toast/saha_alert.dart';
import '../../model/info_address_customer.dart';
import '../../model/shipment_method.dart';

class ShipmentCustomerController extends GetxController {
  InfoAddressCustomer? infoAddressCustomer = InfoAddressCustomer();
  FeeWithTypeShip? feeWithTypeShipInput;
  var shipmentMethodChoose = ShipmentMethod().obs;
  var feeShip = <FeeShip>[].obs;
  int? branchId;
  var listShipper = <ListShipper>[];
  int? partnerShipperId;
  var feeWithTypeShipChoose = FeeWithTypeShip().obs;
  ShipmentCustomerController(
      {this.infoAddressCustomer, this.feeWithTypeShipInput, this.branchId,this.partnerShipperId}) {
    //chargeShipmentFee(idAddressCustomer :infoAddressCustomer!.id,branchId: branchId);
    print("??????? ${partnerShipperId}");
    getListShipper(
        idAddressCustomer: infoAddressCustomer!.id, branchId: branchId);
    if (feeWithTypeShipInput != null) {
      feeWithTypeShipChoose(feeWithTypeShipInput);
    }
  }

  var listShipment = RxList<ShipmentMethod>();
  var isLoadingShipmentMethod = false.obs;

  // Future<void> chargeShipmentFee({int? idAddressCustomer,int? branchId}) async {
  //   isLoadingShipmentMethod.value = true;
  //   try {
  //     var res = await CustomerRepositoryManager.shipmentRepository
  //         .chargeShipmentFee(idAddressCustomer : idAddressCustomer,branchId: branchId);
  //     listShipment(res!.data!.data!);
  //   } catch (err) {
  //     SahaAlert.showError(message: err.toString());
  //   }
  //   isLoadingShipmentMethod.value = false;
  // }

  Future<void> getListShipper({int? idAddressCustomer, int? branchId}) async {
    isLoadingShipmentMethod.value = true;
    try {
      var res = await CustomerRepositoryManager.shipmentRepository
          .getListShipper(
              idAddressCustomer: idAddressCustomer, branchId: branchId);
      listShipper = res!.data!;

      await Future.wait(
          [...listShipper.map((e) => calculateFee(idAddressCustomer :idAddressCustomer,shipperId: e.partnerId!,branchId: branchId))]);
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
    isLoadingShipmentMethod.value = false;
  }

  Future<void> calculateFee(
      {int? idAddressCustomer, int? branchId, required int shipperId}) async {
   
    try {
      var res = await CustomerRepositoryManager.shipmentRepository.calculateFee(
          idAddressCustomer: idAddressCustomer,
          branchId: branchId,
          shipperId: shipperId);
      feeShip.add(res!.data!);
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
    
  }
}
