import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:get/get.dart';

import '../../components/toast/saha_alert.dart';
import '../../utils/store_info.dart';
import '../data_app_controller.dart';

class ProductQrController extends GetxController {
  int? postId;
  int? productId;
  var loadInit = false.obs;
  ProductQrController({this.postId,this.productId}){
    if(StoreInfo().firebaseDynamicLink != null){
      buildLink(postId: postId,productId: productId);
    }
  }
  DataAppCustomerController dataAppCustomerController = Get.find();
  var link = "".obs;
  String linkQr({int? productId, int? postId}) {
    if (productId != null) {
      if (dataAppCustomerController.isLogin.value == true) {
        return 'https://${(dataAppCustomerController.badge.value.domain ?? "") == '' ? "${StoreInfo().getCustomerStoreCode()}.myiki.vn" : dataAppCustomerController.badge.value.domain!.contains('https://') ? dataAppCustomerController.badge.value.domain!.replaceAll('https://', '') : dataAppCustomerController.badge.value.domain}/qr-app?action=product&references_id=${productId}&cid=${dataAppCustomerController.infoCustomer.value.id}';
      } else {
        return 'https://${(dataAppCustomerController.badge.value.domain ?? "") == '' ? "${StoreInfo().getCustomerStoreCode()}.myiki.vn" : dataAppCustomerController.badge.value.domain!.contains('https://') ? dataAppCustomerController.badge.value.domain!.replaceAll('https://', '') : dataAppCustomerController.badge.value.domain}/qr-app?action=product&references_id=${productId}';
      }
    }

    if (postId != null) {
      if (dataAppCustomerController.isLogin.value == true) {
        return 'https://${(dataAppCustomerController.badge.value.domain ?? "") == '' ? "${StoreInfo().getCustomerStoreCode()}.myiki.vn" : dataAppCustomerController.badge.value.domain!.contains('https://') ? dataAppCustomerController.badge.value.domain!.replaceAll('https://', '') : dataAppCustomerController.badge.value.domain}/qr-app?action=post&references_id=${postId}&cid=${dataAppCustomerController.infoCustomer.value.id}';
      } else {
        return 'https://${(dataAppCustomerController.badge.value.domain ?? "") == '' ? "${StoreInfo().getCustomerStoreCode()}.myiki.vn" : dataAppCustomerController.badge.value.domain!.contains('https://') ? dataAppCustomerController.badge.value.domain!.replaceAll('https://', '') : dataAppCustomerController.badge.value.domain}/qr-app?action=post&references_id=${postId}';
      }
    } else {
      return '';
    }
  }

  Future<void> buildLink({int? productId, int? postId}) async {
    loadInit.value = true;
    try {
      final dynamicLinkParams = DynamicLinkParameters(
        link: productId != null
            ? Uri.parse(
                "https://${StoreInfo().getFirebaseDynamicLink()}/customer?phone_number=${dataAppCustomerController.infoCustomer.value.phoneNumber}&cid=${dataAppCustomerController.infoCustomer.value.id}&product_id=$productId")
            : Uri.parse(
                "https://${StoreInfo().getFirebaseDynamicLink()}/customer?phone_number=${dataAppCustomerController.infoCustomer.value.phoneNumber}&cid=${dataAppCustomerController.infoCustomer.value.id}&post_id=$postId"),
        uriPrefix: "https://${StoreInfo().getFirebaseDynamicLink()}",
        androidParameters: AndroidParameters(
            packageName:
                "${dataAppCustomerController.packageInfo.value.packageName}"),
        iosParameters: IOSParameters(
            bundleId:
                "${dataAppCustomerController.packageInfo.value.packageName}",
            appStoreId: StoreInfo().getAppStoreId()),
      );
      final dynamicLink =
          await FirebaseDynamicLinks.instance.buildShortLink(dynamicLinkParams);
      print(dynamicLink.shortUrl);
      link.value = dynamicLink.shortUrl.toString();
    } catch (e) {
      SahaAlert.showError(message: e.toString());
    }finally{
      loadInit.value = false;
    }
  }
}
