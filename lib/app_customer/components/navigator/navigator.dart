import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/const/const_type_message.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/notification_history/all_notification_response.dart';
import 'package:sahashop_customer/app_customer/screen_default/chat_customer_screen/chat_user_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/order_history/order_history_screen.dart';

class NotificationNavigator {
  static void navigator(NotificationCus notificationUser) async {
    if (notificationUser.type == NEW_MESSAGE) {
      Get.to(() => ChatCustomerScreen());
    } else if (notificationUser.type == NEW_ORDER) {
      Get.to(() => OrderHistoryScreen());
    }
  }
}
