import '../../../model/list_shipper.dart';

class ListShipperRes {
  int? code;
  bool? success;
  List<ListShipper>? data;
  String? msgCode;
  String? msg;

  ListShipperRes({
    this.code,
    this.success,
    this.data,
    this.msgCode,
    this.msg,
  });

  factory ListShipperRes.fromJson(Map<String, dynamic> json) => ListShipperRes(
        code: json["code"],
        success: json["success"],
        data: (json["data"] == null || !(json["data"] is List))
            ? []
            : List<ListShipper>.from(
                json["data"]!.map((x) => ListShipper.fromJson(x))),
        msgCode: json["msg_code"],
        msg: json["msg"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "success": success,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
        "msg_code": msgCode,
        "msg": msg,
      };
}
