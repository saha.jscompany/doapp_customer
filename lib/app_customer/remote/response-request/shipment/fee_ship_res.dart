

import '../../../model/fee_ship.dart';

class FeeShipRes {
    int? code;
    bool? success;
    String? msgCode;
    String? msg;
    FeeShip? data;

    FeeShipRes({
        this.code,
        this.success,
        this.msgCode,
        this.msg,
        this.data,
    });

    factory FeeShipRes.fromJson(Map<String, dynamic> json) => FeeShipRes(
        code: json["code"],
        success: json["success"],
        msgCode: json["msg_code"],
        msg: json["msg"],
        data: (json["data"] == null || json["data"] is List) ? null : FeeShip.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "success": success,
        "msg_code": msgCode,
        "msg": msg,
        "data": data?.toJson(),
    };
}

