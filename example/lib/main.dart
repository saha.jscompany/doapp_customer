import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sahashop_customer/app_customer/load_data/load_firebase.dart';
import 'package:sahashop_customer/app_customer/sahashop_customer.dart';

import 'firebase_options.dart';

const STORE_CODE = "khkcare";
const STORE_NAME = "PG SÀN THUỐCVIỆT";

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  try {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    LoadFirebase.initFirebase();
  } catch (e) {
    print(e.toString());
  }
  runApp(SahaShopCustomer(
    storeCode: STORE_CODE,
    storeName: STORE_NAME,
    sloganColor: Colors.black87.withOpacity(0.6),
    slogan: "Sữa Gì Cũng Có",
  ));
}
